//
//  RoundImage.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit

class RoundImage: UIImageView {
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
        self.backgroundColor = UIColor.whiteColor()
        
    }

}
