//
//  Person.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import Foundation
import UIKit

class Person {
    
     var id: String!
     var imgUrl: String!
     var location: String!
     var function: String!
     var email: String!
     var telephoneNumber: String!
     var editionID: String!
     var name: String!
     var tag: String!
     var longtitude: String!
     var latitude: String!
     var image: UIImage
    
    
    init() {
        image = UIImage(named: "defaultImage")!
    }
    
    
    func downloadPicture(completed: DownloadComplete) {
        
        if imgUrl != nil && imgUrl != "" {
            let url = NSURL(string: imgUrl + "200")
            
           
            //SWIFT 3 SYNTAX (I think)
//            
//            let queue = dispatch_queue_create("DownloadPictures", nil)
//            dispatch_async(queue) {
//             
//                if let data = NSData(contentsOfURL: url!), let img = UIImage(data: data) {
//                    self.image = img
//                }
//                completed()
//             
//            }
//            
//



            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if let data = NSData(contentsOfURL: url!) {
                    dispatch_async(dispatch_get_main_queue(), {
                        if let img = UIImage(data: data) {
                            self.image = img
                        }
                        completed()
                    });
                }
            }
            
 
 
 
 
        }
    }
    
    
}
