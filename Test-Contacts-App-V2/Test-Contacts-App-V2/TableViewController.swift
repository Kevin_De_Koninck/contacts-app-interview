//
//  TableViewController.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 24/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import AVFoundation

class TableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var persons = [Person]()
    var filteredPersons = [Person]()
    var inSearchMode = false
    
    var didStartScrolling: Bool = false
    var tableIsLoaded: Bool = false
    
    var scrollSFX: AVAudioPlayer!
    var clickSFX: AVAudioPlayer!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigationcontroller
        navigationController?.navigationBar.barTintColor = UIColor(red: CGFloat(73/255.0), green: CGFloat(177/255.0), blue: CGFloat(222/255.0), alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.title = "Contacts"
        
        // Searchbar
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.Done //set to done instead of 'search'
        searchBar.enablesReturnKeyAutomatically = false //keep done button enabled
        
        // Swiftspinner
        SwiftSpinner.show("Refreshing data ...").addTapHandler({
            SwiftSpinner.hide()
            }, subtitle: "Tap to hide while connecting.")
        
        // Download and parse JSON data
        downloadContacts(downloadContactsCompleted)
        
        //Audio
        initAudio()
        
    }
    

/* AUDIO */
    
    func initAudio() {
        do{
            try scrollSFX = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("rollover", ofType: "wav")!))
            try clickSFX = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("switch", ofType: "wav")!))
            
            scrollSFX.prepareToPlay()
            clickSFX.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    

/* DOWNLOAD AND PARSE JSON DATA */
    
    func downloadContacts(completed: DownloadComplete) {
        
        Alamofire.request(.GET, JSON_URL).responseJSON { response in
            
            if let JSON = response.result.value {
                
                if let array = JSON as? [Dictionary<String, AnyObject>] where array.count > 0 {
                    
                    for people in array{
                        let person =  Person()
                        
                        if let id = people["id"] as? String {
                            person.id = id
                        }
                        if let imgURL = people["imgUrl"] as? String {
                            person.imgUrl = imgURL
                        }
                        if let location = people["location"] as? String {
                            person.location = location
                        }
                        if let function = people["function"] as? String {
                            person.function = function
                        }
                        if let email = people["email"] as? String {
                            person.email = email
                        }
                        if let tel = people["tel"] as? String {
                            person.telephoneNumber = tel
                        }
                        if let editionId = people["editionId"] as? String {
                            person.editionID = editionId
                        }
                        if let name = people["name"] as? String {
                            person.name = name
                        }
                        if let tag = people["tag"] as? String {
                            person.tag = tag
                        }
                        if let lng = people["lng"] as? String {
                            person.longtitude = lng
                        }
                        if let lat = people["lat"] as? String {
                            person.latitude = lat
                        }
                        
                        person.downloadPicture(self.downloadPictureCompleted)
                        self.persons.append(person)
                    }
                    
                    completed()
                }
            }
        }
    }
    
    
    func downloadContactsCompleted() {
        SwiftSpinner.hide()
        tableView.reloadData()
        tableIsLoaded = true
    }
    
    
    func downloadPictureCompleted() {
        tableView.reloadData()
    }

    
/* TABLE VIEW DATA SOURCE */

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode {
            return filteredPersons.count
        } else {
            return persons.count
        }
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("PersonCell", forIndexPath: indexPath) as? PersonCell {
            
            var person_: Person!
            
            if inSearchMode {
                person_ = filteredPersons[indexPath.row]
            } else {
                person_ = persons[indexPath.row]
            }
            
            cell.configureCell(person_)
            
            if didStartScrolling == true {
                scrollSFX.play()
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        // I added this because the scrollSFX sound started playing while filling the table when initialising
        if tableIsLoaded == true {
            didStartScrolling = true
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if inSearchMode {
            performSegueWithIdentifier("DetailsVC", sender: filteredPersons[indexPath.row])
        } else {
            performSegueWithIdentifier("DetailsVC", sender: persons[indexPath.row])
        }
        clickSFX.play()
    }

    
    //override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {}



/* NAVIGATION BETWEEN VC's */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "DetailsVC" {
            if let nextVC = segue.destinationViewController as? DetailsVC {
                
                if let _person = sender as? Person {
                    nextVC.person = _person
                }
                
            }
        }
    }
    
    
    
/* UIVIEW DELEGATE FUNCTIONS */
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
/* SEARCHBAR DELEGATE FUNCTIONS */
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" || searchBar.text == nil {
            inSearchMode = false
            view.endEditing(true)
            
        } else {
            inSearchMode = true
            let lower = searchBar.text!.lowercaseString
            
            //$0: grab the first element
            filteredPersons = persons.filter({$0.name.lowercaseString.rangeOfString(lower) != nil})
        }
        
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }

}
