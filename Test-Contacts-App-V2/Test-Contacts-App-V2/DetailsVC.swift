//
//  DetailsVC.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class DetailsVC: UIViewController {
    
    var person: Person! //get from previous VC
    
    @IBOutlet weak var PersonImageRound: RoundImage!
    @IBOutlet weak var blurLayer: UIVisualEffectView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var functionLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneNrLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var map: MKMapView!
    
    var clickSFX: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // NavigationController
        navigationController?.navigationBar.barTintColor = UIColor(red: CGFloat(73/255.0), green: CGFloat(177/255.0), blue: CGFloat(222/255.0), alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.title = "Details"
        
        initAudio()
        updateUI()
    }
    

    override func viewWillDisappear(animated: Bool) {
        clickSFX.play()
    }
    
    
    func updateUI() {
        
        // Labels
        PersonImageRound.image = person.image
        headerImage.image = person.image
        nameLbl.text = person.name
        functionLbl.text = person.function
        phoneNrLbl.text = person.telephoneNumber
        emailLbl.text = person.email
        locationLbl.text = person.location.stringByReplacingOccurrencesOfString(" - ", withString: "\n")
        
        // Blurring
        blurLayer.effect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        
        // Show Map
        if let lat = Double(person.latitude), let lon = Double(person.longtitude) {
            centerMapOnLocation(lat, longitude: lon, regionRadius: 200)
        } else {
            centerMapOnLocation(0.0, longitude: 0.0, regionRadius: 2000)
        }
    }
    
    
/* AUDIO */
    
    func initAudio() {
        do{
            try clickSFX = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("switch", ofType: "wav")!))
            clickSFX.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    
/* MAPKIT FUNCTIONS */
    
    func centerMapOnLocation(latitude: Double, longitude: Double, regionRadius: CLLocationDistance) {
        
        //zoom in and center on region
        let location = CLLocation(latitude: latitude, longitude: longitude)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: true)
        
        //Set a pin
        let dropPoint = DropPoint(latitude: latitude, longitude: longitude)
        dropPoint.title = "Location of ..."
        dropPoint.subtitle = person.name
        map.addAnnotation(dropPoint)
    }
    
    
/* SHARED APPLICATIONS */
    
    @IBAction func phoneButtonPressed(sender: AnyObject) {
        let number = person.telephoneNumber.stringByReplacingOccurrencesOfString(" ", withString: "").stringByReplacingOccurrencesOfString("+", withString: "00")
        
        clickSFX.play()
        UIApplication.sharedApplication().openURL(NSURL(string: "tel://" + number)!)
    }
    
    
    @IBAction func emailButtonPressed(sender: AnyObject) {
        clickSFX.play()
        UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(person.email)")!)
    }

    

}
