//
//  Station.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

// Used for putting annotations on a mapView

import Foundation
import MapKit

class DropPoint: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var latitude: Double
    var longitude:Double
    
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

}
