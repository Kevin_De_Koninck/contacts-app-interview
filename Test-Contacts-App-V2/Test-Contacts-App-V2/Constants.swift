//
//  Constants.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import Foundation

let JSON_URL: String = "https://dl.dropboxusercontent.com/u/3973453/contacts.json"

typealias DownloadComplete = () -> ()
