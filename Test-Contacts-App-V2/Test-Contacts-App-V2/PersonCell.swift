//
//  PersonCell.swift
//  Test-Contacts-App-V2
//
//  Created by Kevin De Koninck on 25/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet weak var personImg: UIImageView!
    @IBOutlet weak var functionLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        personImg.layer.cornerRadius = 10.0
        personImg.clipsToBounds = true
    }

    
    func configureCell(person: Person) {
        nameLbl.text = person.name
        functionLbl.text = person.function
        personImg.image = person.image
    }

}
