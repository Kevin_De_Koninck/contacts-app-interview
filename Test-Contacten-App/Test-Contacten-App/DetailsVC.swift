//
//  DetailsVC.swift
//  Test-Contacten-App
//
//  Created by Kevin De Koninck on 24/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    
    @IBOutlet weak var personLbl: UILabel!
    @IBOutlet weak var functionLbl: UILabel!
    @IBOutlet weak var LocationLbl: UILabel!
    @IBOutlet weak var telLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var editionID: UILabel!
    @IBOutlet weak var tagLbl: UILabel!
    @IBOutlet weak var longtitudeLbl: UILabel!
    @IBOutlet weak var latitudeLbl: UILabel!
    @IBOutlet weak var ID: UILabel!

    @IBOutlet weak var personPicture: UIImageView!
    var person: Person! //get from previous VC

    override func viewDidLoad() {
        super.viewDidLoad()

        personLbl.text = person.name
        functionLbl.text = person.function
        LocationLbl.text = person.location
        telLbl.text = person.telephoneNumber
        emailLbl.text = person.email
        editionID.text = person.editionID
        tagLbl.text = person.tag
        longtitudeLbl.text = person.longtitude
        latitudeLbl.text = person.latitude
        ID.text = person.id
        personPicture.image = person.image
        
        
    }

    @IBAction func backButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
