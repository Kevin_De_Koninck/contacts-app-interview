//
//  ViewController.swift
//  Test-Contacten-App
//
//  Created by Kevin De Koninck on 24/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var persons = [Person]()
    var filteredPersons = [Person]()
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadContacts(downloadContactsCompleted)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.Done //set to done instead of 'search'
        searchBar.enablesReturnKeyAutomatically = false //keep done button enabled
    }
    
    
    
    func downloadContacts(completed: DownloadComplete) {
        
        Alamofire.request(.GET, JSON_URL)
            .responseJSON { response in
                
                
                if let JSON = response.result.value {
                    
                    if let array = JSON as? [Dictionary<String, AnyObject>] where array.count > 0 {
                        
                        for people in array{
                            let person =  Person()
                            
                            if let id = people["id"] as? String {
                                person.id = id
                            }
                            if let imgURL = people["imgUrl"] as? String {
                                person.imgUrl = imgURL
                            }
                            
                            if let location = people["location"] as? String {
                                person.location = location
                            }
                            
                            if let function = people["function"] as? String {
                                person.function = function
                            }
                            
                            if let email = people["email"] as? String {
                                person.email = email
                            }
                            
                            if let tel = people["tel"] as? String {
                                person.telephoneNumber = tel
                            }
                            
                            if let editionId = people["editionId"] as? String {
                                person.editionID = editionId
                            }
                            
                            if let name = people["name"] as? String {
                                person.name = name
                            }
                            
                            if let tag = people["tag"] as? String {
                                person.tag = tag
                            }
                            
                            if let lng = people["lng"] as? String {
                                person.longtitude = lng
                            }
                            
                            if let lat = people["lat"] as? String {
                                person.latitude = lat
                            }
                        
                            self.persons.append(person)
                            
                        }
                        
                        completed()
                    }
                }
        }
        
    }
    
    func downloadContactsCompleted() {
        
        tableView.reloadData()
        
        for person in persons {
            person.downloadPicture(downloadPictureCompleted)
        }
        
    }
    
    func downloadPictureCompleted() {
        tableView.reloadData()
    }
    
    
    
    
    
    
    
    
    /* UIVIEW DELEGATE FUNCTIONS */
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    /* SEARCHBAR DELEGATE FUNCTIONS */
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" || searchBar.text == nil {
            inSearchMode = false
            view.endEditing(true)
            
        } else {
            inSearchMode = true
            let lower = searchBar.text!.lowercaseString
            
            //$0: grab the first element
            filteredPersons = persons.filter({$0.name.lowercaseString.rangeOfString(lower) != nil})
        }
        
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    
    
    
    
    
    /* TableView Functions */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode {
            return filteredPersons.count
        } else {
            return persons.count
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("DetailsVC", sender: persons[indexPath.row])
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "DetailsVC" {
            
            if let nextVC = segue.destinationViewController as? DetailsVC {
                
                if let _person = sender as? Person {
                    nextVC.person = _person
                }

            }
   
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("PersonCell", forIndexPath: indexPath) as? PersonCell {
            
            
            var person_: Person!
            if inSearchMode {
                person_ = filteredPersons[indexPath.row]
            } else {
                person_ = persons[indexPath.row]
            }

            cell.configureCell(person_)
            
            
            return cell
        } else {
            return UITableViewCell()
        }
        
        
    }


}

