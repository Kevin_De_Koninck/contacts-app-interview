//
//  Person.swift
//  Test-Contacten-App
//
//  Created by Kevin De Koninck on 24/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import Foundation
import UIKit

class Person {
    
    private var _id: String!
    private var _imgUrl: String!
    private var _location: String!
    private var _function: String!
    private var _email: String!
    private var _telephoneNumber: String!
    private var _editionID: String!
    private var _name: String!
    private var _tag: String!
    private var _longtitude: String!
    private var _latitude: String!
    var image: UIImage =  UIImage(named: "defaultImage")!

    
    
    
    
    
    var id: String! {
        get{
            if _id == nil {
                _id = ""
            }
            return _id
        }
        set{
            _id = newValue
        }
    }
    
    var imgUrl: String! {
        get{
            if _imgUrl == nil {
                _imgUrl = ""
            }
            return _imgUrl
        }
        set{
            _imgUrl = newValue
        }
    }
    
    var location: String! {
        get{
            if _location == nil {
                _location = ""
            }
            return _location
        }
        set{
            _location = newValue
        }
    }
    
    var function: String! {
        get{
            if _function == nil {
                _function = ""
            }
            return _function
        }
        set{
            _function = newValue
        }
    }
    
    var email: String! {
        get{
            if _email == nil {
                _email = ""
            }
            return _email
        }
        set{
            _email = newValue
        }
    }
    
    var telephoneNumber: String! {
        get{
            if _telephoneNumber == nil {
                _telephoneNumber = ""
            }
            return _telephoneNumber
        }
        set{
            _telephoneNumber = newValue
        }
    }
    
    var editionID: String! {
        get{
            if _editionID == nil {
                _editionID = ""
            }
            return _editionID
        }
        set{
            _editionID = newValue
        }
    }
    
    var name: String! {
        get{
            if _name == nil {
                _name = ""
            }
            return _name
        }
        set{
            _name = newValue
        }
    }
    
    var tag: String! {
        get{
            if _tag == nil {
                _tag = ""
            }
            return _tag
        }
        set{
            _tag = newValue
        }
    }
    
    var longtitude: String! {
        get{
            if _longtitude == nil {
                _longtitude = ""
            }
            return _longtitude
        }
        set{
            _longtitude = newValue
        }
    }
    
    var latitude: String! {
        get{
            if _latitude == nil {
                _latitude = ""
            }
            return _latitude
        }
        set{
            _latitude = newValue
        }
    }



    init(){
    }
    
    func downloadPicture(completed: DownloadComplete) {
        
        if _imgUrl != nil && _imgUrl != "" {
            let url = NSURL(string: _imgUrl + "125")
        
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if let data = NSData(contentsOfURL: url!) {
                    dispatch_async(dispatch_get_main_queue(), {
                        if let img = UIImage(data: data) {
                            self.image = img
                        }
                        completed()
                    });
                }
            }
        }
    }
    
        
        
        

}

