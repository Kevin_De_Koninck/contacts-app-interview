//
//  PersonCell.swift
//  Test-Contacten-App
//
//  Created by Kevin De Koninck on 24/08/16.
//  Copyright © 2016 Kevin De Koninck. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personFunction: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        personImage.layer.cornerRadius = 5.0
        personImage.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(person: Person) {
        personName.text = person.name
        personFunction.text = person.function
        personImage.image = person.image
     //   personImage.image = UIImage(named: "defaultImage")
    }

}
